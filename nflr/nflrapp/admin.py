from django.contrib import admin
from .models import Ciudad
from .models import Dueño
from .models import Equipo
from .models import Jugador
from .models import Estadio


# Register your models here.

admin.site.register(Ciudad)
admin.site.register(Dueño)
admin.site.register(Equipo)
admin.site.register(Jugador)
admin.site.register(Estadio)
