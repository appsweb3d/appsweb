# Generated by Django 5.0 on 2023-12-11 17:17

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ciudad',
            fields=[
                ('codigo', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Duenio',
            fields=[
                ('codigo', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Equipo',
            fields=[
                ('codigo', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=30)),
                ('venue', models.CharField(max_length=30)),
                ('ciudad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nflrapp.ciudad')),
                ('duenio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nflrapp.duenio')),
            ],
        ),
        migrations.CreateModel(
            name='Estadio',
            fields=[
                ('codigo', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=30)),
                ('capacidad', models.CharField(max_length=30)),
                ('size', models.CharField(max_length=30)),
                ('ciudad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nflrapp.ciudad')),
                ('equipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nflrapp.equipo')),
            ],
        ),
        migrations.CreateModel(
            name='Jugador',
            fields=[
                ('codigo', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=30)),
                ('numero', models.CharField(max_length=3)),
                ('posicion', models.CharField(max_length=30)),
                ('equipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nflrapp.equipo')),
            ],
        ),
    ]
