from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from .models import Ciudad, Duenio, Equipo, Jugador, Estadio
from .forms import CiudadForm, DuenioForm, EquipoForm, JugadorForm, EstadioForm
from django.views import generic

# Create your views here.

def home(request):
    return render(request, "home.html")

#Views para Ciudad
class ListCiudades(generic.View):
    template_name = "ciudades/list_ciudades.html"  #"name of the folder/filename.html"
    context = {}

    def get(self, request):
        self.context = {
            "ciudades": Ciudad.objects.all()
        }
        return render(request, self.template_name, self.context)
    
class CreateCiudad(CreateView):
    template_name = "ciudades/create_ciudades.html"
    model = Ciudad
    form_class = CiudadForm
    success_url = reverse_lazy('nflrapp:list_ciudades')  #('nombre de la app:nombre del template)
    
class DetailCiudad(DetailView):
    template_name = "ciudades/detail_ciudades.html"
    model = Ciudad
    context_object_name = 'ciudad'

class UpdateCiudad(UpdateView):
    template_name = "ciudades/update_ciudades.html"
    model = Ciudad
    form_class = CiudadForm
    success_url = reverse_lazy('nflrapp:list_ciudades')  #('nombre de la app:nombre del template)
    
class DeleteCiudad(DeleteView):
    template_name = "ciudades/delete_ciudades.html"
    model = Ciudad
    success_url = reverse_lazy('nflrapp:list_ciudades')
    

#Views para Dueño  
class ListDuenios(generic.View):
    template_name = "duenios/list_duenios.html"  #"name of the folder/filename.html"
    context = {}

    def get(self, request):
        self.context = {
            "duenios": Duenio.objects.all()
        }
        return render(request, self.template_name, self.context)
    
class CreateDuenios(CreateView):
    template_name = "duenios/create_duenios.html"
    model = Duenio
    form_class = DuenioForm
    success_url = reverse_lazy('nflrapp:list_duenios')  #('nombre de la app:nombre del template)
    
class DetailDuenios(DetailView):
    template_name = "duenios/detail_duenios.html"
    model = Duenio
    context_object_name = 'duenio'

class UpdateDuenios(UpdateView):
    template_name = "duenios/update_duenios.html"
    model = Duenio
    form_class = DuenioForm
    success_url = reverse_lazy('nflrapp:list_duenios')  #('nombre de la app:nombre del template)
    
class DeleteDuenios(DeleteView):
    template_name = "duenios/delete_duenios.html"
    model = Duenio
    success_url = reverse_lazy('nflrapp:list_duenios')  
    
#Views para el Equipo  
class ListEquipos(generic.View):
    template_name = "equipos/list_equipos.html"  #"name of the folder/filename.html"
    context = {}

    def get(self, request):
        self.context = {
            "equipos": Equipo.objects.all()
        }
        return render(request, self.template_name, self.context)
    
class CreateEquipos(CreateView):
    template_name = "equipos/create_equipos.html"
    model = Equipo
    form_class = EquipoForm
    success_url = reverse_lazy('nflrapp:list_equipos')  #('nombre de la app:nombre del template)
    
class DetailEquipos(DetailView):
    template_name = "equipos/detail_equipos.html"
    model = Equipo
    context_object_name = 'equipo'

class UpdateEquipos(UpdateView):
    template_name = "equipos/update_equipos.html"
    model = Equipo
    form_class =   EquipoForm
    success_url = reverse_lazy('nflrapp:list_equipos')  #('nombre de la app:nombre del template)
    
class DeleteEquipos(DeleteView):
    template_name = "equipos/delete_equipos.html"
    model = Equipo
    success_url = reverse_lazy('nflrapp:list_equipos')  
    
#Views para el Jugador  
class ListJugadores(generic.View):
    template_name = "jugadores/list_jugadores.html"  #"name of the folder/filename.html"
    context = {}

    def get(self, request):
        self.context = {
            "jugadores": Jugador.objects.all()
        }
        return render(request, self.template_name, self.context)
    
class CreateJugadores(CreateView):
    template_name = "jugadores/create_jugadores.html"
    model = Jugador
    form_class = JugadorForm
    success_url = reverse_lazy('nflrapp:list_jugadores')  #('nombre de la app:nombre del template)
    
class DetailJugadores(DetailView):
    template_name = "jugadores/detail_jugadores.html"
    model = Jugador
    context_object_name = 'jugador'

class UpdateJugadores(UpdateView):
    template_name = "jugadores/update_jugadores.html"
    model = Jugador
    form_class = JugadorForm
    success_url = reverse_lazy('nflrapp:list_jugadores')  #('nombre de la app:nombre del template)
    
class DeleteJugadores(DeleteView):
    template_name = "jugadores/delete_jugadores.html"
    model = Jugador
    success_url = reverse_lazy('nflrapp:list_jugadores') 
    
#Views para el Estadio  
class ListEstadios(generic.View):
    template_name = "estadios/list_estadios.html"  #"name of the folder/filename.html"
    context = {}

    def get(self, request):
        self.context = {
            "estadios": Estadio.objects.all()
        }
        return render(request, self.template_name, self.context)
    
class CreateEstadios(CreateView):
    template_name = "estadios/create_estadios.html"
    model = Estadio
    form_class = EstadioForm
    success_url = reverse_lazy('nflrapp:list_estadios')  #('nombre de la app:nombre del template)
    
class DetailEstadios(DetailView):
    template_name = "estadios/detail_estadios.html"
    model = Estadio
    context_object_name = 'estadio'

class UpdateEstadios(UpdateView):
    template_name = "estadios/update_estadios.html"
    model = Estadio
    form_class = EstadioForm
    success_url = reverse_lazy('nflrapp:list_estadios')  #('nombre de la app:nombre del template)
    
class DeleteEstadios(DeleteView):
    template_name = "estadios/delete_estadios.html"
    model = Estadio
    success_url = reverse_lazy('nflrapp:list_estadios') 