from django.urls import path
from nflrapp import views

app_name = 'nflrapp'

urlpatterns = [
    path('ciudad/list/', views.ListCiudades.as_view(), name='list_ciudades'),  #'asi se vera el path del buscador, views.name of the view.as_view', name= nombre del path(ese se usa en el base)
    path('ciudad/create/', views.CreateCiudad.as_view(), name='create_ciudades'),
    path('ciudad/detail/<int:pk>/', views.DetailCiudad.as_view(), name='detail_ciudades'),
    path('ciudad/update/<int:pk>/', views.UpdateCiudad.as_view(), name='update_ciudades'),
    path('ciudad/<int:pk>/delete/', views.DeleteCiudad.as_view(), name='delete_ciudades'),

]




#METODOJP
    #path('ciudad/', views.CAMBIARNOMBRE.as_view(), name='CAMBIARNOMBRE'),



    #ENDMETODJP