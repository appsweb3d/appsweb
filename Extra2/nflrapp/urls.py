from django.urls import path
from nflrapp import views

app_name = 'nflrapp'

urlpatterns = [
    path('ciudad/list/', views.ListCiudades.as_view(), name='list_ciudades'),  #'asi se vera el path del buscador, views.name of the view.as_view', name= nombre del path(ese se usa en el base)
    path('ciudad/create/', views.CreateCiudad.as_view(), name='create_ciudades'),
    path('ciudad/detail/<int:pk>/', views.DetailCiudad.as_view(), name='detail_ciudades'),
    path('ciudad/update/<int:pk>/', views.UpdateCiudad.as_view(), name='update_ciudades'),
    path('ciudad/<int:pk>/delete/', views.DeleteCiudad.as_view(), name='delete_ciudades'),
    
    path('duenios/list/', views.ListDuenios.as_view(), name='list_duenios'),
    path('duenios/create/', views.CreateDuenios.as_view(), name='create_duenios'),
    path('duenios/detail/<int:pk>/', views.DetailDuenios.as_view(), name='detail_duenios'),
    path('duenios/update/<int:pk>/', views.UpdateDuenios.as_view(), name='update_duenios'),
    path('duenios/<int:pk>/delete/', views.DeleteDuenios.as_view(), name='delete_duenios'),
    
    path('equipos/list/', views.ListEquipos.as_view(), name='list_equipos'),
    path('equipos/create/', views.CreateEquipos.as_view(), name='create_equipos'),
    path('equipos/detail/<int:pk>/', views.DetailEquipos.as_view(), name='detail_equipos'),
    path('equipos/update/<int:pk>/', views.UpdateEquipos.as_view(), name='update_equipos'),
    path('equipos/<int:pk>/delete/', views.DeleteEquipos.as_view(), name='delete_equipos'),
    
    path('jugadores/list/', views.ListJugadores.as_view(), name='list_jugadores'),
    path('jugadores/create/', views.CreateJugadores.as_view(), name='create_jugadores'),
    path('jugadores/detail/<int:pk>/', views.DetailJugadores.as_view(), name='detail_jugadores'),
    path('jugadores/update/<int:pk>/', views.UpdateJugadores.as_view(), name='update_jugadores'),
    path('jugadores/<int:pk>/delete/', views.DeleteJugadores.as_view(), name='delete_jugadores'),
    
    path('estadios/list/', views.ListEstadios.as_view(), name='list_estadios'),
    path('estadios/create/', views.CreateEstadios.as_view(), name='create_estadios'),
    path('estadios/detail/<int:pk>/', views.DetailEstadios.as_view(), name='detail_estadios'),
    path('estadios/update/<int:pk>/', views.UpdateEstadios.as_view(), name='update_estadios'),
    path('estadios/<int:pk>/delete/', views.DeleteEstadios.as_view(), name='delete_estadios'),
]