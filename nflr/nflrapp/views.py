from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from .models import Ciudad
from .forms import CiudadForm
from django.views import generic

# Create your views here.

def home(request):
    return render(request, "home.html")

#Views para Ciudad
class ListCiudades(generic.View):
    template_name = "ciudades/list_ciudades.html"  #"name of the folder/filename.html"
    context = {}

    def get(self, request):
        self.context = {
            "ciudades": Ciudad.objects.all()
        }
        return render(request, self.template_name, self.context)
    
class CreateCiudad(CreateView):
    template_name = "ciudades/create_ciudades.html"
    model = Ciudad
    form_class = CiudadForm
    success_url = reverse_lazy('nflrapp:list_ciudades')  #('nombre de la app:nombre del template)
    
class DetailCiudad(DetailView):
    template_name = "ciudades/detail_ciudades.html"
    model = Ciudad
    context_object_name = 'ciudad'

class UpdateCiudad(UpdateView):
    template_name = "ciudades/update_ciudades.html"
    model = Ciudad
    form_class = CiudadForm
    success_url = reverse_lazy('nflrapp:list_ciudades')  #('nombre de la app:nombre del template)
    
class DeleteCiudad(DeleteView):
    template_name = "ciudades/delete_ciudades.html"
    model = Ciudad
    success_url = reverse_lazy('nflrapp:list_ciudades')
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    #SOY HACKER YA TE HACKIE XD
#DETAIL
#class DetailCiudad(generic.DetailView):
 #   template_name = "home/detail_ciudad.html"
  #  context = {}
#
 #   def get(self, request, pk):
  #      self.context = {
   #         "ciudad": Ciudad.objects.get(pk=pk)
    #    }
     #   return render(request, self.template_name, self.context)

#class ListCiudades(ListView):
#    template_name = "ciudades/list_ciudades.html"  #"name of the folder/filename.html"
#    model = Ciudad  #name of the model
#    content_object_name = 'ciudades'

