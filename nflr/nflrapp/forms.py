from django import forms
from .models import Ciudad, Dueño, Equipo, Jugador, Estadio

class CiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ["nombre"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de la ciudad "}),
        }