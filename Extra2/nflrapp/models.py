from django.db import models

# Create your models here.

class Ciudad(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    
    def __str__(self):
        return self.nombre
    
class Duenio(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    
    def __str__(self):
        return self.nombre
    
class Equipo(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    venue = models.CharField(max_length=30)
    duenio = models.ForeignKey(Duenio, on_delete=models.CASCADE)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre
    
class Jugador(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    numero = models.CharField(max_length=3)
    posicion = models.CharField(max_length=30)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre
    
class Estadio(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    capacidad = models.CharField(max_length=30)
    size = models.CharField(max_length=30)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre