from django import forms
from .models import Ciudad, Duenio, Equipo, Jugador, Estadio

class CiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ["nombre"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de la ciudad "}),
        }
        
class DuenioForm(forms.ModelForm):
    class Meta:
        model = Duenio
        fields = ["nombre"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del Dueño "}),
        }
        
class EquipoForm(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = ["nombre", "venue", "duenio", "ciudad"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del Equipo "}),
            "venue": forms.TextInput(attrs={"class": "form-control", "placeholder": "Venue "}),
            "duenio": forms.Select(attrs={"class": "form-control"}),
            "ciudad": forms.Select(attrs={"class": "form-control"}), 
        }
        
class JugadorForm(forms.ModelForm):
    class Meta:
        model = Jugador
        fields = ["nombre", "numero", "posicion", "equipo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del Jugador "}),
            "numero": forms.TextInput(attrs={"class": "form-control", "placeholder": "Numero del Jugador "}),
            "posicion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Posicion del Jugador "}),
            "equipo": forms.Select(attrs={"class": "form-control"}), 
        }
        
class EstadioForm(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = ["nombre", "capacidad", "size", "equipo", "ciudad"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del Estadio "}),
            "capacidad": forms.TextInput(attrs={"class": "form-control", "placeholder": "Capacidad del estadio "}),
            "size": forms.TextInput(attrs={"class": "form-control", "placeholder": "Box Size "}),
            "equipo": forms.Select(attrs={"class": "form-control"}), 
            "ciudad": forms.Select(attrs={"class": "form-control"}), 
        }